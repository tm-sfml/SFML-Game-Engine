Developed as a cmake project, compiled using g++ on Windows via WSL (Windows Subsistem for Linux) 


# Features:

- Signatures: 
    - A way to define your Systems and link them to Entities:  
        - Entity has a bitset to mark which components it can provide
        - Signatures are bitsets with bits indicating if a component is needed for signature's system
    - Signature construction occures at compile time, using [brigand](https://github.com/edouarda/brigand) meta-programming library: 
    users only provides a type list of components and tags they've defined, and a type list of type lists for signatures. 
    Then they are able to get the signature bitset by signature's class. 
    Most runtime work is trivial as a good part of data is computed on compilation and hard- as constants for each signature's template specialization. 