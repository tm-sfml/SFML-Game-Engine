//
// Created by Taras Martyniuk on 7/26/2018.
//
#ifndef SFML_GAME_ENGINE_BRIGAND_TYPE_MACRO_H
#define SFML_GAME_ENGINE_BRIGAND_TYPE_MACRO_H

#define BRIGAND_TYPE(T) typename decltype(T)::type


#endif //SFML_GAME_ENGINE_BRIGAND_TYPE_MACRO_H
