/**
 * Created by Taras Martyniuk on 7/29/2018.
 */
#ifndef SFML_GAME_ENGINE_SIGNATURES_H
#define SFML_GAME_ENGINE_SIGNATURES_H

#include <ecs/signatures/Settings.h>
#include <ecs/signatures/Signature.h>

#endif //SFML_GAME_ENGINE_SIGNATURES_H
